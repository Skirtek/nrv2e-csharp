# CLEAN
rm *.nupkg
rm -R src/bin src/obj

# BUILD
dotnet restore src/NRV2E.csproj
msbuild /p:Configuration=Release /t:Clean,Build src/NRV2E.csproj

# PACK
nuget pack NRV2E.nuspec

echo "FINISHED."
